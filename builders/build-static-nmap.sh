#!/bin/bash
# -*- coding: utf-8 -*-
# https://github.com/opsec-infosec/nmap-static-binaries/blob/master/build.sh
set -eux

cat > build.sh << "EOF"
set -e
set -o pipefail
set -x

mkdir /build /output

NMAP_VERSION=7.92
OPENSSL_VERSION=1.1.1k

function build_openssl() {
    cd /build

    # Download
    curl -sSkLO https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz
    tar zxvf openssl-${OPENSSL_VERSION}.tar.gz
    cd openssl-${OPENSSL_VERSION}

    # Configure
    CC='/opt/cross/x86_64-linux-musl/bin/x86_64-linux-musl-gcc -static' ./Configure no-shared linux-x86_64

    # Build
    make
    echo "** Finished building OpenSSL"
}

function build_nmap() {
    cd /build

    # Install Python
    DEBIAN_FRONTEND=noninteractive apt-get update
    DEBIAN_FRONTEND=noninteractive apt-get install -yy python

    # Download
    curl -sSkLO http://nmap.org/dist/nmap-${NMAP_VERSION}.tar.bz2
    tar xjvf nmap-${NMAP_VERSION}.tar.bz2
    cd nmap-${NMAP_VERSION}

    # Configure
    CC='/opt/cross/x86_64-linux-musl/bin/x86_64-linux-musl-gcc -static -fPIC' \
        CXX='/opt/cross/x86_64-linux-musl/bin/x86_64-linux-musl-g++ -static -static-libstdc++ -fPIC' \
        LD=/opt/cross/x86_64-linux-musl/bin/x86_64-linux-musl-ld \
        LDFLAGS="-L/build/openssl-${OPENSSL_VERSION}"   \
        ./configure \
            --without-ndiff \
            --without-zenmap \
            --without-nmap-update \
            --with-pcap=linux \
            --with-openssl=/build/openssl-${OPENSSL_VERSION}

    # Don't build the libpcap.so file
    sed -i -e 's/shared\: /shared\: #/' libpcap/Makefile

    # Build
    make -j4
    /opt/cross/x86_64-linux-musl/bin/x86_64-linux-musl-strip nmap ncat/ncat nping/nping
}

function doit() {
    build_openssl
    build_nmap

    # Copy to output
    if [ -d /output ]
    then
        OUT_DIR=/output/`uname | tr 'A-Z' 'a-z'`/`uname -m`
        mkdir -p $OUT_DIR
        cp /build/nmap-${NMAP_VERSION}/nmap $OUT_DIR/
        cp /build/nmap-${NMAP_VERSION}/ncat/ncat $OUT_DIR/
        cp /build/nmap-${NMAP_VERSION}/nping/nping $OUT_DIR/
        echo "** Finished **"
    else
        echo "** /output does not exist **"
    fi

    mkdir /root/mnt/nmap-${NMAP_VERSION}
    cp -r $OUT_DIR/* /root/mnt/nmap-${NMAP_VERSION}
    cp -r /build/nmap-${NMAP_VERSION}/nselib /root/mnt/nmap-${NMAP_VERSION}
    cp -r /build/nmap-${NMAP_VERSION}/scripts /root/mnt/nmap-${NMAP_VERSION}
    cp -r /build/nmap-${NMAP_VERSION}/{nse_main.lua,nmap-services,nmap-service-probes,nmap-rpc,nmap-protocols,nmap-payloads,nmap-os-db,nmap-mac-prefixes} /root/mnt/nmap-${NMAP_VERSION}
    cp -r /build/nmap-${NMAP_VERSION}/docs/{nmap.xsl,nmap.dtd} /root/mnt/nmap-${NMAP_VERSION}
}

doit
EOF

docker run --rm -ti -e DEBIAN_FRONTEND=noninteractive -v $(pwd):/root/mnt andrewd/musl-cross /bin/bash /root/mnt/build.sh

fn=$(basename $(find . -maxdepth 1 -type d ! -path .))
tar czvf ../$fn.tar.gz $fn/*
sudo rm -rf $fn

rm build.sh
