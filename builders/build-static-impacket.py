# most of the virtualenv code from
# https://stackoverflow.com/questions/57593111/how-to-call-pip-from-a-python-script-and-make-it-install-locally-to-that-script
import subprocess
import tarfile
import shutil
import glob
import sys
import os


class VirtualEnvironment(object):
    def __init__(self, venv_dir):
        self.venv_dir = venv_dir
        if os.name == 'nt':
            self.venv_python = os.path.join(self.venv_dir, 'Scripts', 'python.exe')
        elif os.name == 'posix':
            self.venv_python = os.path.join(self.venv_dir, 'bin', 'python')

    def pip_install(self, package, extra_args=None):
        args = [sys.executable, '-m', 'pip', 'install', package]
        if extra_args:
            args.extend(extra_args)
        subprocess.call(args)

    def create_venv(self):
        self.pip_install('virtualenv')

        if not os.path.exists(self.venv_dir):
            subprocess.call([sys.executable, '-m', 'virtualenv', self.venv_dir])

    def is_venv(self):
        return sys.prefix == self.venv_dir

    def activate_venv(self):
        subprocess.call([self.venv_python, __file__] + sys.argv[1:])
        exit(0)

    def run(self):
        if not self.is_venv():
            self.create_venv()
            self.activate_venv()

def freeze():
    pwd = os.path.dirname(os.path.realpath(__file__))
    venv = VirtualEnvironment(os.path.join(pwd, 'venv'))

    venv.run()
    venv.pip_install('wheel')
    if os.name == 'nt':
        venv.pip_install('pyreadline')
        extra = ["--global-option=build_ext", r"--global-option=--library-dirs=C:\WdpPack\Lib;C:\Program Files (x86)\Windows Kits\10\Lib\10.0.19041.0\um\x64;C:\Program Files (x86)\Windows Kits\10\Lib\10.0.19041.0\ucrt\x64", r"--global-option=--include-dirs=C:\WdpPack\Include;C:\Program Files (x86)\Windows Kits\10\Include\10.0.19041.0\um;C:\Program Files (x86)\Windows Kits\10\Include\10.0.19041.0\shared;C:\Program Files (x86)\Windows Kits\10\Include\10.0.19041.0\ucrt;C:\Program Files (x86)\Windows Kits\10\bin\10.0.19041.0\x64"]
        venv.pip_install('pcapy', extra_args=extra)
    elif os.name == 'posix':
        venv.pip_install('pcapy')
    venv.pip_install('pyinstaller')
    venv.pip_install('impacket')

    from impacket import version
    v = version.version

    try:
        import PyInstaller.__main__
    except Exception as e:
        print('Error:\n{}'.format(e))

    if os.name == 'nt':
        impacket_examples = os.path.join(pwd, 'venv', 'Scripts', '*.py')
        outdir = 'impacket-windows-{}'.format(v)
    elif os.name == 'posix':
        impacket_examples = os.path.join(pwd, 'venv', 'bin', '*.py')
        outdir = 'impacket-linux-{}'.format(v)

    for each in glob.glob(impacket_examples):
        if os.path.basename(each).startswith('activate_this'):
            continue
        if os.name == 'nt':
            outfile = os.path.basename(each)
        elif os.name == 'posix':
            outfile = '{}.bin'.format(os.path.basename(each))

        PyInstaller.__main__.run([
            '--onefile',
            '--clean',
            '--specpath', os.path.join(pwd, 'specs'),
            '--distpath', os.path.join(pwd, outdir),
            '--name', outfile,
            each
        ])

    with tarfile.open('../{}.tar.gz'.format(outdir), 'w:gz') as tar:
        tar.add(outdir)

    for i in [outdir, 'venv', 'build', 'specs']:
        shutil.rmtree(i)

if __name__ == '__main__':
    freeze()

